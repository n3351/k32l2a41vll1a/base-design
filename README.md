# Base design

Minimal functional design for a K32L2A41VLL1A microcontroller.





## Project stage

**Preliminary Hardware Designing**

## Project status

* [ ] Minimal component schematics.
* [ ] Minimal component schematics review.
* [ ] Board component schematics.
* [ ] Board component schematics review.
* [ ] Component selection.
* [ ] Component selection alternative.
* [ ] Footprint association.
* [ ] Footprint review.
* [ ] PCB Rules setup.
* [ ] PCB Rules setup review.
* [ ] Component placing.
* [ ] Component placing review.
* [ ] PCB rougth routing.
* [ ] PCB fine routing. 
* [ ] PCB layout review. 
* [ ] PCB DRC. 
* [ ] Gerbers. 
* [ ] Gerbers review.
* [ ] Hardware production.
* [ ] Hardware tested.




